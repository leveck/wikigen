# wikigen

This is a very simple OnBoardC program to be compiled on a Palm OS device. Its gui has one field, and that one field has the current date surrounded by [[]]. If you use PalmWiki (either the hack, or OS5 version), this will take you to a new memo with the first line being the current date. Perfect for keeping a journal.